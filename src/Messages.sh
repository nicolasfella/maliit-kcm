# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: None
$XGETTEXT $(find . -name \*.cpp -o -name \*.h -o -name \*.qml) -o $podir/kcm_maliit.pot
